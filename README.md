# Welcome to the marvelous FooBar game!

## How can I play it?
Just clone this repo, `cd` into it and press play (`npm play`).

## But...what about the rules?
You can check under the hood with `npm test`, or just _Read the source, Luke_.