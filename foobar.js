const range = require('lodash/range');

const fooBar = num => 
  range(1, num+1)
  .map(elem => (
    elem % 15 === 0 ? 'FooBar' :
    elem % 3 === 0  ? 'Foo' :
    elem % 5 === 0  ? 'Bar' :
    elem
  )
);

module.exports = fooBar;