let assert = require('assert');
let fooBar = require('../foobar');
 
describe("FooBar", function() {
  let result = fooBar(15);

  it("Multiples of 3 should transform into Foo", () => {
    assert.equal(result[2], 'Foo')
    assert.equal(result[5], 'Foo')
  });
  
  it("Multiples of 5 should transform into Bar", () => {
    assert.equal(result[4], 'Bar')
    assert.equal(result[9], 'Bar')
  });
  
  it("Multiples of 3 and 5 should transform into FooBar", () =>
    assert.equal(result[14], 'FooBar')
  );
});

